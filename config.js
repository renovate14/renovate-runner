module.exports = {
    endpoint: 'https://gitlab/api/v4/',
    platform: 'gitlab',
    persistRepoData: true,
    logLevel: 'debug',
    onboardingConfig: {
        extends: ['renovate/renovate-config'], // reference to config project that we created
    },
    autodiscover: true,
};
